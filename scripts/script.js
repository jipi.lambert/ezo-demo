window.addEventListener('scroll', function(e) {
    let menu = document.getElementById('menu');
    if(window.scrollY >= 33 ){
        menu.classList.add('menu--white') 
    } else if (window.scrollY <= 33 ) {
        menu.classList.remove('menu--white');
    }
});

function fakeSlider(){
    let hero = document.getElementById('hero');
    setInterval(function(){
        if(hero.classList.contains("bg1")){
            hero.classList.add("bg2");
            hero.classList.remove("bg1");
        } else{
            hero.classList.add("bg1");
            hero.classList.remove("bg2");
        }      
    }, 7000);
}

fakeSlider();

document.addEventListener( 'DOMContentLoaded', function () {
	new Splide( '#image-slider',{
        type   : 'loop',
        perPage: 3,
	    rewind : true,
    }).mount();
} );

var scroll = new SmoothScroll('a[href*="#"]');
