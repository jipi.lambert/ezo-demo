const { src, dest, watch } = require('gulp');
const sass = require('gulp-sass')(require('sass'));

function compile_sass(){
    return src("./scss/**/*.scss")
        .pipe(sass())
        .pipe(dest('./style/'));
}

exports.default = function(){
    watch("./scss/**/*.scss", compile_sass);
}